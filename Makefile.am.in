## Makefile.am.in - process this file to produce Makefile.am
## Copyright (C) 2015-2021 Alex Vong <alexvong1995 AT protonmail DOT com>
##
## This program is free software; you can redistribute it and/or
## modify it under the terms of the GNU General Public License
## as published by the Free Software Foundation; either version 2
## of the License, or (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program; if not, write to the Free Software Foundation,
## Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

ACLOCAL_AMFLAGS = -I m4

SRCS = @SRCS@

TESTS = tests/spot-check tests/fermat-test tests/self-test
dist_check_SCRIPTS = $(TESTS)

EXTRA_DIST = .debian upstream Makefile.am.in
dist_noinst_SCRIPTS = bootstrap doc/manpage.pl scripts/mlucas.in
dist_man_MANS = doc/mlucas.1

bin_SCRIPTS = mlucas primenet

mlucas: scripts/mlucas.in
	$(AM_V_GEN) $(SED) \
	-e 's,[@]pkglibexecdir[@],$(pkglibexecdir),g' \
	-e 's,[@]host_cpu[@],$(host_cpu),g' \
	< scripts/mlucas.in > mlucas \
	&& $(CHMOD) +x mlucas

primenet: ./upstream/primenet.py
	$(AM_V_GEN) $(SED) \
	-e 's,#!/usr/bin/env python,#!/usr/bin/python3,' \
	 <./upstream/primenet.py >./primenet \
	&& $(CHMOD) +x ./primenet

pkglibexec_PROGRAMS =

if BUILD_GENERIC_C
pkglibexec_PROGRAMS += mlucas-generic-c
mlucas_generic_c_SOURCES = $(SRCS)
mlucas_generic_c_CFLAGS = $(EXTRACFLAGS)
mlucas_generic_c_LDFLAGS = $(EXTRALDFLAGS)
mlucas_generic_c_LIBS = $(EXTRALIBS)
endif

if BUILD_SSE2
pkglibexec_PROGRAMS += mlucas-sse2
mlucas_sse2_SOURCES = $(SRCS)
mlucas_sse2_CPPFLAGS = -DUSE_SSE2
mlucas_sse2_CFLAGS = $(EXTRACFLAGS) -msse2
mlucas_sse2_LDFLAGS = $(EXTRALDFLAGS)
mlucas_sse2_LIBS = $(EXTRALIBS)
endif

if BUILD_AVX
pkglibexec_PROGRAMS += mlucas-avx
mlucas_avx_SOURCES = $(SRCS)
mlucas_avx_CPPFLAGS = -DUSE_AVX
mlucas_avx_CFLAGS = $(EXTRACFLAGS) -mavx
mlucas_avx_LDFLAGS = $(EXTRALDFLAGS)
mlucas_avx_LIBS = $(EXTRALIBS)
endif

if BUILD_AVX2
pkglibexec_PROGRAMS += mlucas-avx2
mlucas_avx2_SOURCES = $(SRCS)
mlucas_avx2_CPPFLAGS = -DUSE_AVX2
mlucas_avx2_CFLAGS = $(EXTRACFLAGS) -mavx2
mlucas_avx2_LDFLAGS = $(EXTRALDFLAGS)
mlucas_avx2_LIBS = $(EXTRALIBS)
endif

if BUILD_AVX512
pkglibexec_PROGRAMS += mlucas-avx512
mlucas_avx512_SOURCES = $(SRCS)
mlucas_avx512_CPPFLAGS = -DUSE_AVX512
mlucas_avx512_CFLAGS = $(EXTRACFLAGS) -mavx512f -mavx512cd
mlucas_avx512_LDFLAGS = $(EXTRALDFLAGS)
mlucas_avx512_LIBS = $(EXTRALIBS)
endif

if BUILD_ARM_V8_SIMD
pkglibexec_PROGRAMS += mlucas-arm-v8-simd
mlucas_arm_v8_simd_SOURCES = $(SRCS)
mlucas_arm_v8_simd_CPPFLAGS = -DUSE_ARM_V8_SIMD
mlucas_arm_v8_simd_CFLAGS = $(EXTRACFLAGS)
mlucas_arm_v8_simd_LDFLAGS = $(EXTRALDFLAGS)
mlucas_arm_v8_simd_LIBS = $(EXTRALIBS)
endif

MOSTLYCLEANFILES = *.cfg
CLEANFILES = mlucas
