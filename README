* [[https://www.mersenneforum.org/mayer/README.html][Mlucas 20.1.1]]

mlucas is an open-source (and free/libre) program for performing Lucas-Lehmer
test on prime-exponent Mersenne numbers, that is, integers of the form
2 ^ p - 1, with prime exponent p. In short, everything you need to search for
world-record Mersenne primes! It has been used in the verification of various
Mersenne primes, including the 45th, 46th, 48th, 49th and 50th found Mersenne
prime.

You may use it to test any suitable number as you wish, but it is preferable
that you do so in a coordinated fashion, as part of the Great Internet Mersenne
Prime Search ([[https://www.mersenne.org/primes/][GIMPS]]).

** Usage

[[https://www.mersenneforum.org/mayer/README.html][Online documentation]] is available.
The help text can be found in the source code file:./upstream/help.txt
On a Debian system, the help text is also installed as
file:/usr/share/doc/mlucas/help.txt.gz

** Installation

Run

#+BEGIN_SRC
./configure
make
make check
make install
#+END_SRC

to complete the installation.

Normally, the configure script should guess your system configuration.
However, you can always override it by passing flags to the configure script.

Running test is optional. Run it if you want to be on the safe side.

Below is a summary of notable options understood by the configure script.
You can see all options by running ~./configure -h~.

** Options

Run

#+BEGIN_SRC
./configure -h
#+END_SRC

to see all options.

** Licensing

Most of the files are licensed under the GNU General Public License version 2
or later. Other files are under permissive licenses or in the public domain.

See file:COPYING for details regarding licensing.

** Bugs

[FIXME: This section is potentially outdated]

The argument parser is buggy. The relative position of arguments is relevant
to mlucas, the order of arguments in SYNOPSIS in mlucas(1) should be followed
to avoid confusing the parser. Only 100, 1000 and 10000 are supported for
iters flag. However, the parser will not reject unsupported arguments. Using
unsupported arguments for -iters flag may trigger strange behaviour.

For problems regarding the program mlucas, please contact the author
Ernst W. Mayer <ewmayer AT aol DOT com>.
For installation and documentation related problems regarding this package
and this file:README, please contact
Alex Vong <alexvong1995 AT protonmail DOT com>.

See BUGS section in mlucas(1) for details.

** ROADMAP

[FIXME: This section is potentially outdated]

- simplify to reduce maintenance burden
  + replace autogen by guile
  + remove useless, duplicate files

- documentation
  + use org-mode in various plain text files
  + replace pod with something lispy

- configure script
  + use sane defaults
  + remove normal cflags, tricky flags
  + detect if compilers support certain flags in configure script

- respect freedesktop standard
  + use file:~/.local/share/mlucas/ instead of file:~/.mlucas.d/

- revert previous changes
  + replace use of ~mlucas_fopen()~ by ~fopen()~
  + and use ~chdir()~ instead
  + replace use of ~system("mkdir -p ...")~ with ~mkdir_p()~ from busybox
  + hence we can remove ~shell_quote()~ and friends

- find out memory bugs
  + asan
  + ubsan
  + valgrind

** Copyright

README - readme for Mlucas
Copyright (C) 2015-2021 Alex Vong <alexvong1995 AT protonmail DOT com>

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software Foundation,
Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
