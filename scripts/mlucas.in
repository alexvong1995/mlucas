#!/bin/sh
#
# mlucas - shell wrapper for Mlucas
# Copyright (C) 2015-2021  Alex Vong <alexvong1995 AT protonmail DOT com>
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

# use error
set -e

# substitute variables
HOST_CPU='@host_cpu@'
PKGLIBEXECDIR='@pkglibexecdir@'

# Obtain DIRNAME by tranforming `foo/bar' to `foo'
# Otherwise, set it to $PKGLIBEXECDIR
case "$0" in
    */*)
        DIRNAME=`expr "x$0" : 'x\(.*\)/[^/]*'`
        ;;
    *)
        DIRNAME="$PKGLIBEXECDIR"
        ;;
esac

# try invoking different variants of mlucas
# FIXME: should be generated
case "$HOST_CPU" in
    i?86)
        if test -x "$DIRNAME/mlucas-sse2" && \
                "$DIRNAME/mlucas-sse2" \
                    -fftlen 192 -iters 100 -radset 0 \
                    >/dev/null 2>&1
        then
            exec "$DIRNAME/mlucas-sse2" "$@"
        elif test -x "$DIRNAME/mlucas-generic-c"
        then
            exec "$DIRNAME/mlucas-generic-c" "$@"

        elif test -x "$PKGLIBEXECDIR/mlucas-sse2" && \
                "$PKGLIBEXECDIR/mlucas-sse2" \
                    -fftlen 192 -iters 100 -radset 0 \
                    >/dev/null 2>&1
        then
            exec "$PKGLIBEXECDIR/mlucas-sse2" "$@"
        elif test -x "$PKGLIBEXECDIR/mlucas-generic-c"
        then
            exec "$PKGLIBEXECDIR/mlucas-generic-c" "$@"
        fi
        ;;

    x86_64)
        if test -x "$DIRNAME/mlucas-avx512" && \
                "$DIRNAME/mlucas-avx512" \
                    -fftlen 192 -iters 100 -radset 0 \
                    >/dev/null 2>&1
        then
            exec "$DIRNAME/mlucas-avx512" "$@"
        elif test -x "$DIRNAME/mlucas-avx2" && \
                "$DIRNAME/mlucas-avx2" \
                    -fftlen 192 -iters 100 -radset 0 \
                    >/dev/null 2>&1
        then
            exec "$DIRNAME/mlucas-avx2" "$@"
        elif test -x "$DIRNAME/mlucas-avx" && \
                "$DIRNAME/mlucas-avx" \
                    -fftlen 192 -iters 100 -radset 0 \
                    >/dev/null 2>&1
        then
            exec "$DIRNAME/mlucas-avx" "$@"
        elif test -x "$DIRNAME/mlucas-sse2" && \
                "$DIRNAME/mlucas-sse2" \
                    -fftlen 192 -iters 100 -radset 0 \
                    >/dev/null 2>&1
        then
            exec "$DIRNAME/mlucas-sse2" "$@"
        elif test -x "$DIRNAME/mlucas-generic-c"
        then
            exec "$DIRNAME/mlucas-generic-c" "$@"

        elif test -x "$PKGLIBEXECDIR/mlucas-avx512" && \
                "$PKGLIBEXECDIR/mlucas-avx512" \
                    -fftlen 192 -iters 100 -radset 0 \
                    >/dev/null 2>&1
        then
            exec "$PKGLIBEXECDIR/mlucas-avx512" "$@"
        elif test -x "$PKGLIBEXECDIR/mlucas-avx2" && \
                "$PKGLIBEXECDIR/mlucas-avx2" \
                    -fftlen 192 -iters 100 -radset 0 \
                    >/dev/null 2>&1
        then
            exec "$PKGLIBEXECDIR/mlucas-avx2" "$@"
        elif test -x "$PKGLIBEXECDIR/mlucas-avx" && \
                "$PKGLIBEXECDIR/mlucas-avx" \
                    -fftlen 192 -iters 100 -radset 0 \
                    >/dev/null 2>&1
        then
            exec "$PKGLIBEXECDIR/mlucas-avx" "$@"
        elif test -x "$PKGLIBEXECDIR/mlucas-sse2" && \
                "$PKGLIBEXECDIR/mlucas-sse2" \
                    -fftlen 192 -iters 100 -radset 0 \
                    >/dev/null 2>&1
        then
            exec "$PKGLIBEXECDIR/mlucas-sse2" "$@"
        elif test -x "$PKGLIBEXECDIR/mlucas-generic-c"
        then
            exec "$PKGLIBEXECDIR/mlucas-generic-c" "$@"
        fi
        ;;

    aarch64)
        if test -x "$DIRNAME/mlucas-arm-v8-simd" && \
                "$DIRNAME/mlucas-arm-v8-simd" \
                    -fftlen 192 -iters 100 -radset 0 \
                    >/dev/null 2>&1
        then
            exec "$DIRNAME/mlucas-arm-v8-simd" "$@"
        elif test -x "$DIRNAME/mlucas-generic-c"
        then
            exec "$DIRNAME/mlucas-generic-c" "$@"

        elif test -x "$PKGLIBEXECDIR/mlucas-arm-v8-simd" && \
                "$PKGLIBEXECDIR/mlucas-arm-v8-simd" \
                    -fftlen 192 -iters 100 -radset 0 \
                    >/dev/null 2>&1
        then
            exec "$PKGLIBEXECDIR/mlucas-arm-v8-simd" "$@"
        elif test -x "$PKGLIBEXECDIR/mlucas-generic-c"
        then
            exec "$PKGLIBEXECDIR/mlucas-generic-c" "$@"
        fi
        ;;

    *)
        if test -x "$DIRNAME/mlucas-generic-c"
        then
            exec "$DIRNAME/mlucas-generic-c" "$@"

        elif test -x "$PKGLIBEXECDIR/mlucas-generic-c"
        then
            exec "$PKGLIBEXECDIR/mlucas-generic-c" "$@"
        fi
        ;;
esac

# report error and die
printf '%s\n%s\n%s\n' \
       'None of the mlucas executables works.' \
       'See BUGS section in mlucas(1) on how to report bugs about' \
       'installation problems.' \
       >&2
exit 1
